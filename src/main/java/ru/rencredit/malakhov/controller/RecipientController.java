package ru.rencredit.malakhov.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.malakhov.api.service.entity.IRecipientService;
import ru.rencredit.malakhov.entity.Recipient;
import ru.rencredit.malakhov.exception.empty.EmptyIdException;
import ru.rencredit.malakhov.exception.empty.EmptyRecipientException;
import ru.rencredit.malakhov.exception.exist.ExistRecipientException;

@Controller
@RequestMapping("/recipient")
public class RecipientController {

    @NotNull
    private final IRecipientService recipientService;

    @Autowired
    public RecipientController(@NotNull final IRecipientService recipientService) {
        this.recipientService = recipientService;
    }

    @GetMapping("/create")
    public String createRecipient(final Model model){
        model.addAttribute("recipient",new Recipient());
        return "recipient_edit";
    }

    @PostMapping("/save/{id}")
    public String saveRecipient(@ModelAttribute("recipient") @NotNull final Recipient recipient, final Model model)
            throws EmptyRecipientException, ExistRecipientException {
        model.addAttribute("recipients",recipientService.save(recipient));
           return "redirect:/recipients";
    }

    @GetMapping("/edit/{id}")
    public String editRecipient(@PathVariable("id") @Nullable final String id, final Model model)
            throws EmptyIdException {
        model.addAttribute("recipient",recipientService.findById(id));
        return "recipient_edit";
    }

    @GetMapping("/delete/{id}")
    public String deleteRecipient(@PathVariable @Nullable final String id, final Model model) throws EmptyIdException {
        model.addAttribute("recipients",recipientService.deleteById(id));
        return "redirect:/recipients";
    }

}