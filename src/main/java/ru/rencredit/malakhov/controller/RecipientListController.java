package ru.rencredit.malakhov.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import ru.rencredit.malakhov.api.service.entity.IRecipientService;

@Controller
public class RecipientListController {

    @NotNull
    private final IRecipientService recipientService;

    public RecipientListController(@NotNull final IRecipientService recipientService) {
        this.recipientService = recipientService;
    }

    @RequestMapping("/recipients")
    public String viewRecipientList(final Model model){
        model.addAttribute("recipients", new ReactiveDataDriverContextVariable(
                recipientService.findAll()));
        return "recipients";
    }

}