package ru.rencredit.malakhov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TeaSchedulerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeaSchedulerApplication.class,args);
    }

}