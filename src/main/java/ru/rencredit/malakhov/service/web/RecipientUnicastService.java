package ru.rencredit.malakhov.service.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import ru.rencredit.malakhov.api.service.web.IRecipientUnicastService;
import ru.rencredit.malakhov.entity.Recipient;

@Service
public class RecipientUnicastService implements IRecipientUnicastService {

    @NotNull
    private ConnectableFlux<Recipient> processor;

    @NotNull
    private Sinks.Many<Recipient> buffer = Sinks.many().multicast().onBackpressureBuffer();

    @Override
    public void onNext(Recipient next) {
        buffer.tryEmitNext(next);
    }

    @Override
    public Flux<Recipient> getMessages() {
        return processor.publish().autoConnect();
    }

}