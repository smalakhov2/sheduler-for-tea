package ru.rencredit.malakhov.service.message;

import org.jetbrains.annotations.NotNull;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.rencredit.malakhov.api.service.entity.IRecipientService;
import ru.rencredit.malakhov.api.service.message.IMessageService;
import ru.rencredit.malakhov.constant.MessageConst;
import ru.rencredit.malakhov.entity.Recipient;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MessageService implements IMessageService {

    @NotNull
    private final IRecipientService recipientService;

    @NotNull
    private final JavaMailSender javaMailSender;

    public MessageService(@NotNull IRecipientService recipientService,
                          @NotNull JavaMailSender javaMailSender)
    {
        this.recipientService = recipientService;
        this.javaMailSender = javaMailSender;
    }

    private void sendMessage(@NotNull final SimpleMailMessage message) {
        javaMailSender.send(message);
    }

    @Override
    public void sentMessage() {
        @NotNull final SimpleMailMessage teaMessage = new SimpleMailMessage();
        @NotNull final String[] emails = Objects.requireNonNull(recipientService.findAllEmail()
                .map(Recipient::getEmail)
                .collect(Collectors.toList())
                .block())
                .toArray(new String[]{});
        teaMessage.setFrom(MessageConst.EMAIL_FROM);
        teaMessage.setSubject(MessageConst.MESSAGE_SUBJECT);
        teaMessage.setText(MessageConst.MESSAGE_TEXT);
        teaMessage.setTo(emails);
        sendMessage(teaMessage);
    }

}