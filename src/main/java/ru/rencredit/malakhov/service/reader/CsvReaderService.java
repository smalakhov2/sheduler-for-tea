package ru.rencredit.malakhov.service.reader;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rencredit.malakhov.api.service.reader.ICsvReaderService;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Service
public class CsvReaderService implements ICsvReaderService {

    @NotNull
    private final String defaultCsvPathname;

    public CsvReaderService(@Value("src/test/resources/WEB-INF/data.csv") @NotNull final String defaultCsvPathname) {
        this.defaultCsvPathname = defaultCsvPathname;
    }

    @NotNull
    @Override
    public List<String[]> readFromResources() throws IOException, CsvException {
        final var reader = new CSVReader(new FileReader(defaultCsvPathname));
        return reader.readAll();
    }

}