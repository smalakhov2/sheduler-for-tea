package ru.rencredit.malakhov.service.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.malakhov.api.service.entity.IRecipientService;
import ru.rencredit.malakhov.entity.Recipient;
import ru.rencredit.malakhov.exception.empty.EmptyIdException;
import ru.rencredit.malakhov.exception.empty.EmptyRecipientException;
import ru.rencredit.malakhov.repository.RecipientRepository;

import java.util.Objects;

@Service
public class RecipientService implements IRecipientService {

    @NotNull
    private final RecipientRepository recipientRepository;

    public RecipientService(@NotNull RecipientRepository recipientRepository) {
        this.recipientRepository = recipientRepository;
    }

    @NotNull
    @Transactional(readOnly = true)
    public Flux<Recipient> findAll() {
        return recipientRepository.findAll();
    }

    @NotNull
    @Transactional(readOnly = true)
    public Flux<Recipient> findAllEmail() {
      return recipientRepository.findALLEmail();
    }

    @Transactional(rollbackForClassName = {"Exception"})
    public @Nullable Mono<Recipient> save(@NotNull final Recipient recipient) throws EmptyRecipientException {
        if (recipient.getEmail().isEmpty()) throw new EmptyRecipientException();
        return recipientRepository.save(recipient);
    }

    @Transactional(readOnly = true)
    public Mono<Recipient> findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return recipientRepository.findById(id);
    }

    @Transactional(rollbackForClassName = {"Exception"})
    public Mono<Recipient> deleteById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return recipientRepository.findById(id).switchIfEmpty(Mono.empty()).filter(Objects::nonNull).
                flatMap(dbRecipient ->  recipientRepository.deleteById(id).then(Mono.just(dbRecipient)));
    }

    @Transactional(rollbackForClassName = {"Exception"})
    public void deleteAll() {
        recipientRepository.deleteAll();
    }

}