package ru.rencredit.malakhov.service.schedule;

import com.opencsv.exceptions.CsvException;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.rencredit.malakhov.api.service.reader.ICsvReaderService;
import ru.rencredit.malakhov.api.service.schedule.IScheduleService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class ScheduleService implements IScheduleService {

    @NotNull
    private final ICsvReaderService csvReaderService;

    public ScheduleService(@NotNull ICsvReaderService csvReaderService) {
        this.csvReaderService = csvReaderService;
    }

    @Override
    public boolean sendingIsToday() throws IOException, CsvException {
        String[] day = getAllSchedules()
                .stream().skip(1)
                .filter(i -> i[0].equals(Integer.toString(LocalDate.now().getYear())))
                .map(s -> s[LocalDate.now().getMonthValue()])
                .findFirst().orElse("null")
                .replace("*", "")
                .replace("+", "")
                .split(",");
        return Arrays.stream(day).noneMatch(s -> s.equals(Integer.toString(LocalDate.now().getDayOfMonth())));
    }

    @NotNull
    @Override
    public List<String[]> getAllSchedules() throws IOException, CsvException {
        return csvReaderService.readFromResources();
    }

}