package ru.rencredit.malakhov.service.schedule;

import com.opencsv.exceptions.CsvException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.rencredit.malakhov.api.service.message.IMessageService;
import ru.rencredit.malakhov.api.service.schedule.IScheduleService;
import ru.rencredit.malakhov.constant.SchedulerConst;

import java.io.IOException;

@Service
public class SchedulerTaskService {

    @NotNull
    private final IMessageService messageService;

    @NotNull
    private final IScheduleService scheduleService;

    @Autowired
    public SchedulerTaskService(@NotNull final IMessageService messageService,
                                @NotNull final IScheduleService scheduleService) {
        this.messageService = messageService;
        this.scheduleService = scheduleService;
    }

    @Scheduled(cron = SchedulerConst.TEA_SCHEDULE)
    public void sentMessage() throws IOException, CsvException {
        if (scheduleService.sendingIsToday()) messageService.sentMessage();
    }

}