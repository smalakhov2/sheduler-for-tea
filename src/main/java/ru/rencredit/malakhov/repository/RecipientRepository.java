package ru.rencredit.malakhov.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import reactor.core.publisher.Flux;
import ru.rencredit.malakhov.entity.Recipient;

@EnableMongoRepositories
public interface RecipientRepository extends ReactiveMongoRepository<Recipient,String> {

    @Query(value = "{}", fields = "{email:1,_id:0}")
    Flux<Recipient> findALLEmail();

}