package ru.rencredit.malakhov.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.malakhov.exception.AbstractException;

public class InvalidScheduleYearException extends AbstractException {

    public InvalidScheduleYearException(@NotNull String message) {
        super(message);
    }

}