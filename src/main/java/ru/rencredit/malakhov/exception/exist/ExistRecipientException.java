package ru.rencredit.malakhov.exception.exist;

import ru.rencredit.malakhov.exception.AbstractException;

public class ExistRecipientException extends AbstractException {


    public ExistRecipientException() {
        super("Error! Recipient is exist already...");
    }
}
