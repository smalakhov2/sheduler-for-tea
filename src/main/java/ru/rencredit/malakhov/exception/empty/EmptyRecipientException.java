package ru.rencredit.malakhov.exception.empty;

import ru.rencredit.malakhov.exception.AbstractException;

public class EmptyRecipientException extends AbstractException {

    public EmptyRecipientException() {
        super("Error! Recipient is empty...");
    }

}