package ru.rencredit.malakhov.exception.empty;

import ru.rencredit.malakhov.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}