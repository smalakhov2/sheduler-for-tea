package ru.rencredit.malakhov.exception.empty;

import ru.rencredit.malakhov.exception.AbstractException;

public class EmptyUsernameException extends AbstractException {

    public EmptyUsernameException() {
        super("Error! Username is empty...");
    }

}