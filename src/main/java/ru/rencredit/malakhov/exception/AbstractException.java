package ru.rencredit.malakhov.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends Exception {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable exception) {
        super(message, exception);
    }

}