package ru.rencredit.malakhov.constant;

public interface MessageConst {

    String EMAIL_FROM = "smalakhov2@rencredit.ru";

    String MESSAGE_SUBJECT = "Приглашение на чаепитие.";

    String MESSAGE_TEXT = "Пора пить чай!";

}