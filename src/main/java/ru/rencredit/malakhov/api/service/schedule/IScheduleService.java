package ru.rencredit.malakhov.api.service.schedule;

import com.opencsv.exceptions.CsvException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

public interface IScheduleService {

    boolean sendingIsToday() throws IOException, CsvException;

    @NotNull
    List<String[]> getAllSchedules() throws IOException, CsvException;

}