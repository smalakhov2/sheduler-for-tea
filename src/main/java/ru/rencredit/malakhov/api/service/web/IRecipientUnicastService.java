package ru.rencredit.malakhov.api.service.web;

import reactor.core.publisher.Flux;
import ru.rencredit.malakhov.entity.Recipient;

public interface IRecipientUnicastService {

    void onNext(Recipient next);

    Flux<Recipient> getMessages();

}