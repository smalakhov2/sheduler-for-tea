package ru.rencredit.malakhov.api.service.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.malakhov.entity.Recipient;
import ru.rencredit.malakhov.exception.empty.EmptyIdException;
import ru.rencredit.malakhov.exception.empty.EmptyRecipientException;
import ru.rencredit.malakhov.exception.exist.ExistRecipientException;

public interface IRecipientService {

    @NotNull
    @Transactional(readOnly = true)
    Flux<Recipient> findAll();

    @NotNull
    @Transactional(readOnly = true)
    Flux<Recipient> findAllEmail();

    @Nullable
    @Transactional(rollbackForClassName = {"Exception"})
    Mono<Recipient> save(@NotNull final Recipient recipient) throws EmptyRecipientException, ExistRecipientException;

    @Transactional(readOnly = true)
    Mono<Recipient> findById(@Nullable final String id) throws EmptyIdException;

    @Transactional(rollbackForClassName = {"Exception"})
    Mono<Recipient> deleteById(@Nullable final String id) throws EmptyIdException;

}