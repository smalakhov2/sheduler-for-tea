package ru.rencredit.malakhov.api.service.reader;

import com.opencsv.exceptions.CsvException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

public interface ICsvReaderService {

    @NotNull
    List<String[]> readFromResources() throws IOException, CsvException;

}