package ru.rencredit.malakhov.entity;

import lombok.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "app_recipient")
public class Recipient {

    @Id
    @NonNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String firstname,lastname;

    @NonNull
    @Indexed
    private String email;

}