package ru.rencredit.malakhov.service.message;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import ru.rencredit.malakhov.constant.MessageConst;
import ru.rencredit.malakhov.entity.Recipient;
import ru.rencredit.malakhov.exception.empty.EmptyRecipientException;
import ru.rencredit.malakhov.exception.exist.ExistRecipientException;
import ru.rencredit.malakhov.service.entity.RecipientService;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MessageServiceTest {

    @NotNull
    @Autowired
    private RecipientService recipientService;

    @Autowired
    private MessageService messageService;

    @BeforeEach
    public void initData() {
        recipientService.deleteAll();
    }

    final Recipient recipient1 = new Recipient("1", "1", "1", "smalakhov2@rencredit.ru");

    @NotNull
    final SimpleMailMessage teaMessage = new SimpleMailMessage();

    @Test
    void sentMessageTest() {
        teaMessage.setFrom(MessageConst.EMAIL_FROM);
        teaMessage.setTo(recipient1.getEmail());
        teaMessage.setSubject(MessageConst.MESSAGE_SUBJECT);
        teaMessage.setText(MessageConst.MESSAGE_TEXT);
        Assertions.assertEquals(MessageConst.EMAIL_FROM, teaMessage.getFrom());
        Assertions.assertEquals(MessageConst.MESSAGE_SUBJECT, teaMessage.getSubject());
        Assertions.assertEquals(MessageConst.MESSAGE_TEXT, teaMessage.getText());
        Assertions.assertDoesNotThrow(()->messageService.sentMessage());
    }
}