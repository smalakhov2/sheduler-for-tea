package ru.rencredit.malakhov.service.reader;

import com.opencsv.exceptions.CsvException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CsvReaderServiceTest {

    @Autowired
    CsvReaderService csvReaderService;

    @Test
    void readFromResourcesTest() throws IOException, CsvException {
        Assertions.assertDoesNotThrow(()-> csvReaderService.readFromResources());
        Assertions.assertNotNull(csvReaderService.readFromResources().get(1));
    }

}