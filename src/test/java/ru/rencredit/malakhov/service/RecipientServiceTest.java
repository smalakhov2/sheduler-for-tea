package ru.rencredit.malakhov.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.malakhov.entity.Recipient;
import ru.rencredit.malakhov.exception.empty.EmptyIdException;
import ru.rencredit.malakhov.exception.empty.EmptyRecipientException;
import ru.rencredit.malakhov.exception.exist.ExistRecipientException;
import ru.rencredit.malakhov.repository.RecipientRepository;
import ru.rencredit.malakhov.service.entity.RecipientService;
import ru.rencredit.malakhov.service.message.MessageService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SpringBootTest
class RecipientServiceTest {

    @NotNull
    @Autowired
    private RecipientService recipientService;

    @Autowired
    RecipientRepository recipientRepository;

    @Autowired
    MessageService messageService;
    /*
        private final Recipient recipient1 = new Recipient("1","1","1","1@1");
        private final Recipient recipient2 = new Recipient("2","2","2","2@2");

        @BeforeEach
        public void initData() throws ExistRecipientException, EmptyRecipientException {
            recipientService.deleteAll();
            recipientService.save(recipient1);
        }

        @AfterEach
        public void cleanData() {
            recipientService.deleteAll();
        }

        @Test
        void findAll() throws ExistRecipientException, EmptyRecipientException {
            Assertions.assertEquals(1,recipientService.findAll().size());
            recipientService.save(recipient2);
            Assertions.assertEquals(2,recipientService.findAll().size());
        }
    */
    @Test
    void findAllEmail() throws ExistRecipientException, EmptyRecipientException {
        messageService.sentMessage();
    }
/*
    @Test
    void findById() throws EmptyIdException {
        Assertions.assertThrows(EmptyIdException.class,()->recipientService.findById(""));
        String findRecipient = Objects.requireNonNull(recipientService.findById(recipient1.getId()).block()).getId();
        Assertions.assertEquals(recipient1.getId(),findRecipient);
    }
    @Test
    void deleteById() throws EmptyIdException {
          Assertions.assertEquals(1,recipientService.findAll().size());
          recipientService.deleteById(recipient1.getId());
          Assertions.assertEquals(null,recipientService.findById(recipient1.getId()));
    }*/
}