FROM openjdk:8-jdk
MAINTAINER Sergei Malakhov <smalakhov2@rencredit.ru>

COPY ./target/scheduler-for-tea.jar ./
EXPOSE 8080
ENTRYPOINT exec /usr/local/openjdk-8/bin/java -jar scheduler-for-tea.jar.jar